import { useState } from "react";
import { Col, Row, Button, Container } from "reactstrap";
import LogIn from "./LogIn";
import SignUp from "./SignUp";

export default function Title(){
    const [displayLogIn,setDisplayLogIn]= useState(1)
    
    const logInBtnClick = ()=>{
        setDisplayLogIn(1)
    }
    const signUpBtnClick = ()=>{
        setDisplayLogIn(0)
    }
    return(
        <div >
            <Container style={{width:"500px"}}>
            <Row >
                <Col xs={6} className="p-0">
                    <Button 
                        color={displayLogIn?"success":"secondary"} style={{width:"100%"}} 
                        onClick={logInBtnClick}
                    >Log In</Button>
                </Col>
                <Col xs={6} className="p-0">
                    <Button 
                        color={!displayLogIn?"success":"secondary"}
                        style={{width:"100%"}}
                        onClick={signUpBtnClick}
                    >Sign Up </Button>
                </Col>
            </Row>
            </Container>
            {displayLogIn?<LogIn></LogIn>: <SignUp></SignUp>}

            
        </div>
    )
}