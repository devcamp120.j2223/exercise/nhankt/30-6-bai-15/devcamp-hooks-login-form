import { useState,useEffect } from "react";
import { Button, Col, Container, Input, Row } from "reactstrap";

export default function SignUp(){
    const [fNameinput,setFNameinput] = useState("")
    const [lNameinput,setLNameinput] = useState("")
    const [email,setEmail] = useState("")
    const [pass,setPass] = useState("")
    const [validate, setValidate] = useState("")

    const handleFNameChange = (event)=>{
        setFNameinput(event.target.value)
    }
    const handleLNameChange = (event)=>{
        setLNameinput(event.target.value)
    }
    const handleEmailChange = (event)=>{
        setEmail(event.target.value)
    }
    const handlePassChange = (event)=>{
        setPass(event.target.value)
    }

    const checkValidEmail = ()=>{
        if (email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/))
            {
                return true
            }
        else {console.log("email không hợp lệ"); return false}
    }
    const checkValidPass = ()=>{
        let result = true 
        if (pass.length >10 || pass.length<3) {console.log("pass too long or short"); result=false}
        return result
    }

    const checkValidFName = () =>{
        let result = true 
        if (fNameinput.length >10 || fNameinput.length<3) {console.log("Fname too long or short"); result=false}
        return result
    }
    const checkValidLName = () =>{
        let result = true 
        if (lNameinput.length >10 || lNameinput.length<3) {console.log("LName too long or short"); result=false}
        return result
    }
    const handleBtnClick = ()=>{
        setValidate(checkValidEmail() && checkValidPass()&&checkValidFName()&&checkValidLName())
        
    }
    useEffect(()=>{
        if (validate=="") return;
        else if (validate) {console.log("email :" + email + "-----" + "pass : "+pass+  "-----" +  "FName : "+ fNameinput+  "-----" + "LName : "+ lNameinput)}
        else {console.log("fail")}
    },[validate])


  
    return(
        <Container className="text-center" style={{width:"500px"}}>
            <h1 className="mb-4">Sign Up for Free</h1>
            <Row className="mb-3">
                <Col xs={6}>
                    <Input placeholder={"First name"} value={fNameinput} onChange={handleFNameChange} ></Input>
                </Col>
                <Col xs={6}>
                    <Input placeholder={"Last name"} value={lNameinput} onChange={handleLNameChange}></Input>
                </Col>
            </Row>
            <Input placeholder={"Email Address"} value={email} onChange={handleEmailChange} className="mb-3"></Input>
            <Input placeholder={"Set Password "} value={pass} onChange={handlePassChange} type="password" className="mb-3"></Input>
            <Row>
                <Button onClick={handleBtnClick}>GET STARTED</Button>
            </Row>
            
        </Container>
)
}