import { useEffect, useState } from "react";
import { Button, Col, Container, Input, Row } from "reactstrap";

export default function LogIn(){
    const [email,setEmail] = useState("")
    const [pass,setPass] = useState("")
    const [validate,setValidate] = useState("")
    

    const handleChangeEmail = (event)=>{
        setEmail(event.target.value)
    }

    const handleChangePass = (event)=>{
        setPass(event.target.value)
    }
    const checkValidEmail = ()=>{
        if (email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/))
            {
                return true
            }
        else {console.log("email không hợp lệ"); return false}
    }
    const checkValidPass = ()=>{
        let result = true 
        if (pass.length >10 || pass.length<3) {console.log("pass too long or short"); result=false}
        return result
    }
    const handleBtnClick = ()=>{
        setValidate(checkValidEmail() && checkValidPass())
        
    }
    useEffect(()=>{
        if (validate=="") return;
        else if (validate) {console.log("email :" + email + "-----" + "pass : "+pass)}
        else {console.log("fail")}
    },[validate])

    
    return(
        <Container className="text-center" style={{width:"500px"}}>
            <h1 className="mb-4">Wellcome Back!</h1>
            <Input placeholder={"Email Address"} place className="mb-3" onChange={handleChangeEmail} value={email}></Input>
            <Input placeholder={"Password "} type="password" className="mb-3" onChange={handleChangePass} value={pass}></Input>
            <Row style={{textAlign:"right"}}>
                <Col >
                    <a href="#" style={{textDecoration: "none"}}>Forgot Password</a>
                </Col>
            </Row>
            <Row>
                <Button onClick={handleBtnClick}>LogIn</Button>
            </Row>
            
        </Container>
)
}